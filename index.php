<!doctype html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">
<title>Taylor Eaves - Portfolio</title>
<script src="js/main.js"></script>
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link href="css/myLightbox.css" rel="stylesheet" type="text/css" />

<?php include('scripts/connect.php');?>

</head>
<body>
	<div name="te" id="mainHeader">
		<a href="#te" class="active"><img src="img/core/logo-te.png" alt="Taylor Eaves"/></a>
		<div class="topnav" id="pageNav">
		<a href="#te" class="active">&nbsp;</a>
		<a href="#aboutme">about</a>
		<a href="#pande">projects & experience</a>
		<a href="#contactme">contact</a>
		<a href="javascript:void(0);" class="icon" onclick="myMenu()">&#9776;</a>
		</div>
	</div>
<div id="contentContainer">
<div id="welcomeSlogan">
	<img src="img/about_me.png" alt="Taylor Eaves"/>
	<div class="slogan">
		<p>hi. i’m <br>
			taylor eaves</p>
			<p class ="title">host and designer</p>
	</div>
</div>
<div id="sectionHeader"><p id="sectionHeaderTitle"><a name="aboutme">about me</a></p></div>
<div name="aboutme" id="aboutMe">
	<?php
//select all from table
	$data_select = "SELECT * FROM tbl_about ORDER BY `about_id` ASC";
	//$sql="SELECT Lastname,Age FROM Persons ORDER BY Lastname";
	if ($result=mysqli_query($link,$data_select))
	 {
	 // Fetch one and one row
	 while ($row=mysqli_fetch_row($result))
		 {
		 printf ("<p>$row[1]</p>");
		 }
	 // Free result set
	 mysqli_free_result($result);
	}
?>
</div>

	<div id="sectionHeader"><p id="sectionHeaderTitle"><a name="pande">projects & experience</a></p></div>
	<div id="mainContent">
  <?php
//select all from table
  $data_select = "SELECT * FROM tbl_work ORDER BY `work_id` DESC";
  //$sql="SELECT Lastname,Age FROM Persons ORDER BY Lastname";
  if ($result=mysqli_query($link,$data_select))
   {
   // Fetch one and one row
   while ($row=mysqli_fetch_row($result))
     {
     printf ("<div id=\"projectTask\">
		 <div id=\"projectImg\"><img src=\"img/work/thumb/$row[6]\" onclick=\"openMyLightBox();currentWork($row[0])\" alt=\"$row[1]\"/>
		 </div>
		 <div id=\"projectName\"><p>$row[1]</p></div>
		 <div id=\"projectType\"><p>$row[3]</p></div>
		 <div id=\"projectGenre\"><p>$row[4]</p></div>
		 <div id=\"projectDate\"><p>$row[2]</p></div>
		 <!-- <div id=\"projectDesc\"><p>$row[5]</p></div> -->
		 </div>");
		 }
   // Free result set
   mysqli_free_result($result);
  }
?>
</div>
<?php
//select all from table
	$data_select = "SELECT * FROM tbl_work ORDER BY `work_id` ASC";
	//$sql="SELECT Lastname,Age FROM Persons ORDER BY Lastname";
	if ($result=mysqli_query($link,$data_select))
	 {
		 printf ("
		 <div id=\"myLightBox\" class=\"myLightBox\">
		 <span class=\"close cursor\" onclick=\"closeMyLightBox()\">CLOSE X</span>
		 <a class=\"prev\" onclick=\"plusMyWork(-1)\">&#10094;</a>
	 	<a class=\"next\" onclick=\"plusMyWork(1)\">&#10095;</a>
	 	<div class=\"caption-container\">
	 	</div>
		 <div class=\"myLightBoxViewer\">");
	 // Fetch one and one row
	 while ($row=mysqli_fetch_row($result))
		 {
		 printf ("<div class=\"myWork\">
		 <img src=\"img/work/full/$row[7]\">
		 <div class=\"caption-container\">
		 <p>$row[0] / 27</p>
		 <p id=\"caption\">$row[1]</p>
		 <p id=\"caption\">$row[5]</p>
		 </div>
		 </div>
		 ");
		 }

	printf ("</div>
	</div>");
	 // Free result set
	 mysqli_free_result($result);
	}

?>
<div id="sectionHeader"><p id="sectionHeaderTitle"><a name="contactme">let's get in touch</a></div>
<div id="contactMe">
	<?php
	//if "email" variable is filled out, send email
	  if (isset($_POST['email']))  {

	  //Email information
	  $admin_email = "tayloreaves@eavesmultimedia.ca";
	  $email = $_POST['email'];
		$phone = $_POST['phone'];
		$name = $_POST['name'];
	  $subject = "Information Request - tayloreaves.com";
	  $comment = $_REQUEST['message'];
		$comment .= "From: ";
		$comment .= $name;
		$comment .= "Phone Number: ";
		$comment .= $phone;


	  //send email
	  mail($admin_email, "$subject", $comment, "From:" . $email);

	  //Email response
		print '<script type="text/javascript">';
print 'alert("Your message has been sent. I will respond shortly")';
print '</script>';
	  }

	  //if "email" variable is not filled out, display the form
	  else  {
	?>


	<form method="post">
	<!--Make sure to give each input a name attribute(name="")-->
		 <label>name: </label><br>
		 <input name="name" type="text" value="your name"size="30" maxlength="50" /><br>
		 <label>email: </label><br>
		 <input name="email" type="text" value="your email address" size="30" maxlength="50" /><br>
		 <label>phone number: </label><br>
		 <input name="phone" type="text" value="your phone number"size="30" maxlength="30" /><br>
		 <label for="message">message: </label><br>
		 <textarea name="message" value="your phone number">your message</textarea><br>
		 <input name="submit" type="submit" value="send" />
	</form>

	<?php
	  }
	?>
</div>

<div id="mainFooter">
		<div id="socialMedia">
			<a href="https://www.linkedin.com/in/taylor-eaves/" target="_blank">
			<img src="img/linkedin-white.png" width="32" height="32" alt="Visit me on LinkedIn">
			</a>
			<a href="https://www.facebook.com/taylor.eaves.39" target="_blank">
			<img src="img/facebook-white.png" width="32" height="32" alt="Visit us on Facebook">
			</a>
			<a href="www.linkedin.com/in/taylor-eaves/" target="_blank">
			<img src="img/instagram-white.png" width="32" height="32" alt="Visit us on Instagram">
			</a>
		</div>
		<div id="copyright">
			<p>copyright 2017 - taylor eaves</p>
		</div>
	</div>
</body>
</html>
