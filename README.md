# Taylor Eaves Portfolio #

This README is for Taylor Eaves' hosted portfolio website.

---

### What is this repository for? ###

This is the source for the www.tayloreaves.com website.

---

### Who do I talk to? ###

Feel free to contact Taylor Eaves if you have any questions, comments or concerns.