-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jan 15, 2018 at 04:26 AM
-- Server version: 5.6.35
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_portfolio`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_about`
--

CREATE TABLE `tbl_about` (
  `about_id` smallint(8) UNSIGNED NOT NULL,
  `about_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_about`
--

INSERT INTO `tbl_about` (`about_id`, `about_desc`) VALUES
(1, 'i am an undergraduate student studying media, theory and production (mtp) at both western university and fanshawe college, where i will obtain a dual degree after this four-year degree-diploma program. prior to this, i went to vanier college in montreal, quebec where i obtained a diploma in communications and media arts.'),
(2, 'while completing my degree, i am also a coordinator and in-game host for mustang athletics at western university. i organize and supervise all western mustangs athletic events and entertain the crowd with interviews and games. after noticing the quality of my work, the organizers of the 2016 arcelormittal vanier cup asked me to be the in-game host for this event, which i have done for two consecutive years now.'),
(3, 'in 2016, i was the program assistant and graphic designer for voboc, a foundation for young adults who have been diagnosed with cancer. i was responsible for creating all graphic content, such as brochures, posters, business cards, etc. in 2015, i was the communications coordinator for football quebec. i was responsible for the media coordination of the two biggest football tournaments in canada, all graphic designs, social media platforms, and post-game interviews. '),
(4, 'apart from my work experience, i am fluently bilingual in both french and english. i am knowledgeable in softwares, such as adobe photoshop, illustrator, after effects and dreamweaver. I am also comfortable with atom, cinema 4d, and sql. i love working with people and being in an active, fast-paced work environment. '),
(5, 'fun facts: \r\ni have a dog named dakota, \r\ni love football, \r\ni was the class of 2012 valedictorian, \r\ni lived in montreal my entire life, \r\nand i would eat pasta 24/7 if that was not unhealthy and fattening.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work`
--

CREATE TABLE `tbl_work` (
  `work_id` smallint(5) UNSIGNED NOT NULL,
  `work_name` text NOT NULL,
  `work_date` date NOT NULL,
  `work_org` varchar(100) NOT NULL,
  `work_type` varchar(32) NOT NULL,
  `work_desc` text NOT NULL,
  `work_thumb` varchar(45) NOT NULL DEFAULT 'default_thumb.png',
  `work_img` varchar(45) NOT NULL DEFAULT 'default.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_work`
--

INSERT INTO `tbl_work` (`work_id`, `work_name`, `work_date`, `work_org`, `work_type`, `work_desc`, `work_thumb`, `work_img`) VALUES
(1, 'audi poster', '2014-03-21', 'vanier college', 'design', 'this is another project i was assigned a car project. i had to choose a car brand and design a poster that reflects it and i chose audi because it is my favourite brand. i believe that the simplicity and modernity of the poster reflects the brand well. this design was made in illustrator and photoshop.', 'audi-poster_thumb.png', 'audi-poster_full.png'),
(2, 'intern at bombardier aerospace', '2014-05-05', 'bombardier aerospace', 'Administration', 'working at bombardier aerospace was my first \"serious\" job. i was hired as an intern for the learjet 85 team. i answered phone calls, filed documents, and made updates to our weekly meeting presentation (depending on progress). later, i was put in charge of market research, which i really enjoyed.', 'bombardier-logo_thumb.png', 'bombardier-logo_full.png'),
(3, '\"be\" poster', '2014-09-15', 'vanier college', 'design', 'when i was in cegep at vanier college, i was assigned the word \"be\" and had to design something that was related to that word. i chose to outline a human figure with all of the words that someone can aspire to be. this design was made in illustrator.', 'be-poster_thumb.png', 'be-poster_full.png'),
(4, 'spalding cup schedule', '2015-05-18', 'football québec', 'design', 'at football québec, we were hosting a  football tournament for 15 and 17 year olds called the spalding cup. i was told to make a schedule for the tournament highlighting the matchups so that fans could follow it. this schedule was made in illustrator.', 'schedule-coupe-spalding_thumb.png', 'schedule-coupe-spalding_full.png'),
(5, 'football québec press release', '2015-05-29', 'football québec', 'communications', 'i was in charge of write press releases on a weekly basis throughout the summer. this is a press release that i wrote in french, that is highlighting the tryouts for team québec in preparation for the canada cup. all press releases were written in french and english. i also designed the header banner for the press release. this was written in microsoft word.', 'fq-pr_thumb.png', 'fq-press-release_full.png'),
(6, 'football quebec\'s canada cup schedule poster ', '2015-07-06', 'football québec', 'design', 'at football québec, we were hosting the biggest nation-wide tournament in canada called the canada cup. this tournament had the best players who were 18 years or younger competing against one another. the tournament was held in québec that year so i had to create a poster promoting the match-ups. this poster was printed on a 36x48\" styrene plastic board, displayed at the entrance of the stadium. it was designed in illustrator.', 'schedule-coupe-canada_thumb.png', 'schedule-coupe-canada_full.png'),
(7, 'team quebec video: day 1', '2015-07-13', 'football québec', 'video', 'this is a video that was created to promote the canada cup and get fans excited for the big tournament - it was posted on football quebec\'s social media. this video is a recap of team quebec training and practicing in preparation for the canada cup. this video was made in after effects.', 'coupe-canada-video_thumb.png', 'coupe-canada-video_full.png'),
(8, 'post-game interview', '2015-07-30', 'football québec', 'photo', 'after every game in the canada cup, i was the post-game host and had to interview the mvp of each opponent in front of the crowd and on camera. this is a photo of myself that was captured as a interviewed the mvp of the saskatchewan.', 'fq-photo_thumb.png', 'fq-photo_full.jpg'),
(9, 'wolf design', '2015-09-29', 'fanshawe college - western university', 'design, illustration', 'in my first year of design at western and fanshawe, i was asked to draw something detailed and that had some sort of significance. i chose to draw a wolf with leaves coming out of it because wolves are my favourite animals; they are leaders and live in the mountains. at the bottom it is written \"move mountains\", which is a powerful statement that signifies leadership. the hand drawn design was then brought into photoshop and illustrator.', 'wolf-design_thumb.png', 'wolf-design_full.png'),
(10, 'graffiti', '2015-10-05', 'fanshawe college - western university', 'design, illustration', 'in my first year of design at western and fanshawe, i was asked to draw something that could resemble graffiti and be put on a wall. i drew hands that each represented a popular social media platform and then had them bound together by tight rope. this drawing was meant to signify that we are being sucked in by these platforms that are trapping and controlling us. the drawing was brought into illustrator and photoshop, where it was digitized and positioned on a photo of myself, taken by kiija gargarello. ', 'graffiti_thumb.png', 'graffiti_full.jpg'),
(11, 'peak logo', '2015-12-07', 'fanshawe college - western university', 'design', 'for my first integrated project at fanshawe college, i had to design a website that reflected some of the photography that i\'ve done. this is the logo that i made for the brand i invented for this integrated project. this logo was designed in illustrator.', 'peak-logo_thumb.png', 'peak-logo_full.png'),
(12, 'vie logo', '2016-03-14', 'fanshawe college - western university', 'design', 'in a group of three, we were asked to create a brand for a new home speaking device that facilitated chores around the house, similar to google home and nest. we chose to call ours \"vie\", which means life in french. this is the logo that we designed for this vie brand and it was designed in illustrator.', 'vie-logo_thumb.png', 'vie-logo_full.png'),
(13, 'vie style guide', '2016-03-14', 'fanshawe college - western university', 'design', 'this is the style guide that i created for the \"vie\" home device that kiija gargarello, connor clark and i invented. this style guide ensures that the brand image remains consistent. it includes things like colors, fonts and logo spacing. this style guide was designed in illustrator.', 'vie-style-guide_thumb.png', 'vie-style-guide_full.png'),
(14, 'vie poster', '2016-03-14', 'fanshawe college - western university', 'design', 'this is a poster that is supposed to reflect the vie brand. the life lines that are attached to the logo on either side is supposed to reflect the idea that this device is your life (since vie is the french translation of life). this design was intended to be simplistic but meaningful. it was entire designed in illustrator.', 'vie-poster_thumb.png', 'vie-poster_full.png'),
(15, 'run voboc run poster', '2016-05-30', 'the voboc foundation', 'design', 'this is a poster that i created to promote the run voboc run marathon collaborating with montreal marathon. this poster was displayed across the city and on social media. it was created in illustrator.', 'voboc-run-poster_thumb.png', 'voboc-run-poster_full.jpg'),
(16, 'voboc brochure', '2016-06-13', 'the voboc foundation', 'design', 'voboc\'s branding material was outdated and the founder asked me to redesign it, one being the brochure. in the old brochure, there was just arial text on an orange or white background. in the redesign, i made sure to include more visuals and colors, to ensure that the brochure catches peoples attention. this brochure was designed in photoshop and illustrator. ', 'voboc-brochure_thumb.png', 'voboc-brochure_full.png'),
(17, 'voboc think-tank invitation poster', '2016-06-20', 'the voboc foundation', 'design, events', 'the think-tank was something that shawna rutledge, doreen edward and i created an initiative called the voboc think-tank, which is an event where young cancer patients and/or cancer survivors get an invitation to join a panel and answer questions about their experience. this is a poster inviting these young individuals to the 2016 think-tank. this invitation was designed in illustrator.', 'voboc-think-tank_thumb.png', 'voboc-think-tank_full.jpg'),
(18, 'snap your pak contest design', '2016-07-04', 'the voboc foundation', 'design', 'snap your pak is a social media contest that i created in order to increase visibility and activity on voboc\'s page, and get followers engaged. the instructions are on the design. this contest was posted on all of voboc\'s social media pages and was designed in illustrator.', 'voboc-snap-poster_thumb.png', 'voboc-snap-poster_full.png'),
(19, 'youth 2 youth (y2y) initiative poster', '2016-07-18', 'The VOBOC Foundation', 'Design', 'the voboc initiative called youth 2 youth (y2y) was created by shawna rutledge, doreen edward and i, with the goal to increase relationships between youth who have been battling with cancer. this poster was to promote the initiative and was designed in illustrator. ', 'voboc-y2y_thumb.png', 'voboc-y2y_full.png'),
(20, 'western mustangs \"5 things you need to know\"', '2016-09-12', 'western university athletics', 'host, video', 'for every football game, the western mustangs would release a video mid-week of myself listing off the five things people need to know for the upcoming game. in the week prior to the homecoming football game vs. the ottawa geegees, danika dos santos and i listed off the 5 things fans need to know before attending. i did all video editing in after effects.', 'western-video_thumb.png', 'western-video_full.png'),
(21, '2016 vanier cup host', '2016-11-26', 'u sports', 'host', 'in 2016, the organizer of the 2016 arcelormittal vanier cup saw me hosting a football game at western university and had asked me if i could host for the vanier cup. i happily said yes and was in charge of in-game promotions and interviews. this is a fan\'s video recording of myself on the big screen at the 2016 vanier cup, highlighting some of the award winners. ', 'vanier-cup-2016_thumb.png', 'vanier-cup-2016_full.png'),
(22, 'jackt\'s officialize timeline', '2017-09-21', 'fanshawe college - western university', 'project management', 'as the project manager of a fanshawe team project working with officialize media, i created a project timeline to ensure that jackt is able to have all of their deliverables ready on time. every aspect of the project is highlighting in a clear list format. this timeline was made in microsoft word.', 'officialize-timeline_thumb.png', 'officialize-timeline_full.png'),
(23, 'western mustangs host', '2017-10-02', 'western university athletics', 'host, photo', 'this is a photo taken by brandon vandecaveye of myself entertaining at a western mustangs home game.in this photo i am telling the crowd to make some noise for the two contestants building a giant puzzle on the football field.', 'western-photo_thumb.png', 'western-photo_full.jpg'),
(24, 'jackt\'s ledc brand guide ', '2017-10-05', 'fanshawe college - western university', 'design', 'working with ledc to promote london, ontario as a city that offers a prosperous future to those wanting to work in the tech industry, my team and i have created a brand named techLondon. In order to ensure that the brand remains consistent, i designed a brand guide highlighting all brand elements that must be followed in order to remain consistent, such as colors, fonts, logos and social media. this brand guide was designed in illustrator.', 'ledc-brand-guide_thumb.png', 'ledc-brand-guide_full.png'),
(25, '2017 vanier cup host ', '2017-11-25', 'u sports', 'host', 'for the second consecutive year, i was asked to host the 2017 vanier cup. this job included conducting interviews, promoting sponsors and providing entertainment for the crowd. this is a fan\'s video recording of myself cheering on two contestants participating in a race on the tim hortons football field.', 'vanier-cup-2017_thumb.png', 'vanier-cup-2017_full.png'),
(26, 'soccer scene', '2017-12-01', 'fanshawe college - western university', 'video, animation', 'this soccer scene was built in cinema 4d and after effects for fanshawe\'s 3d motion course. the individual elements were provided to me by my teacher. the scene, lighting and camera angles were entirely built by myself in cinema 4d. then, the renders were brought into after effects, where effects, lighting and sound was added.', 'soccer-scene_thumb.png', 'soccer-scene_full.png'),
(27, 'car commercial', '2017-12-08', 'fanshawe college - western university', 'video, animation', 'this is a ferrari commercial that was designed for my 3d motion class. the original build of the car was provided by my teacher. all textures and camera angles were done by myself in cinema 4d and then the renders were brought into after effects, where music, lighting and mask effects were added to make the final product. ', 'car-commercial_thumb.png', 'car-commercial_full.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_about`
--
ALTER TABLE `tbl_about`
  ADD PRIMARY KEY (`about_id`);

--
-- Indexes for table `tbl_work`
--
ALTER TABLE `tbl_work`
  ADD PRIMARY KEY (`work_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_about`
--
ALTER TABLE `tbl_about`
  MODIFY `about_id` smallint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_work`
--
ALTER TABLE `tbl_work`
  MODIFY `work_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
