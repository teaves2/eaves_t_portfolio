function myMenu() {
  var x = document.getElementById("pageNav");
  if (x.className === "topnav") {
      x.className += " responsive";
  } else {
      x.className = "topnav";
  }
}

function openMyLightBox() {
  document.getElementById('myLightBox').style.display = "block";
}

function closeMyLightBox() {
  document.getElementById('myLightBox').style.display = "none";
}

var workIndexNum = 1;
showMyWork(workIndexNum);

function plusMyWork(n) {
  showMyWork(workIndexNum += n);
}

function currentWork(n) {
  showMyWork(workIndexNum = n);
}

function showMyWork(n) {
  var i;
  var slides = document.getElementsByClassName("myWork");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {workIndexNum = 1}
  if (n < 1) {workIndexNum = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[workIndexNum-1].style.display = "block";
  dots[workIndexNum-1].className += " active";
  captionText.innerHTML = dots[workIndexNum-1].alt;
}
